# Gemini Client for Android

I read about the [Gemini Protocol](gemini://gemini.circumlunar.space/) some days ago and noticed that there was no native client for Android.
The protocol is really simple, so I decided to have a go at implementing it in a Language I didn't (and still don't) know (Kotlin). This client is far from complete, but it works to a certain degree and can serve as a client for Android until there is something real.
I am not good at programming, especially not with stuff that has a GUI and even less with adding networking functionality to it. I expect many flaws to be in my code and so should you, if you use it. Expect crashes on a regular basis.
If anyone finds this interesting, I might actually put a little more of my time into it, but until then, this will probably stay as-is.

If you want to download the Application without building it yourself, you can download the latest APK [from the releases](https://framagit.org/waweic/gemini-client/-/releases), although I do not recommend this.
Alternatively, you could probably download the latest bleeding-edge build [here](https://framagit.org/waweic/gemini-client/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?job=assembleDebug)
## Stuff that works
* Requesting information via Gemini
* Viewing formatted text/gemini documents
  - Clickable links
  - Bullet points
  - Headings
* Viewing other text/* documents
* Open application when gemini:// link in other app / browser is clicked
* Open other application when a link is clicked that has a scheme the client can't handle
* Basic redirection
* Does not start a new Activity when you click anywhere (thanks to a much appreciated hint by [@utf8equalsX@fosstodon.org](https://fosstodon.org/@utf8equalsX))
* Avoiding redirection loops
* The "Back"-button
* An (internal and temporary) browsing history
* Gemini INPUT
* Connecting to hosts with self signed certificates

## Stuff that doesn't work (aka wishlist for the future)
* Certain SSL/TLS Versions on older Android versions (I probably can't fix this)
* Downloading anything that is not of type text/*
* Crashes when page is too big (feels just like a "real" web browser! #inabiaf)
* Caching for offline use
* Useful error messages
* Graceful error handling
* Markdown
* Code quality / structure
* Client certificates
* Security
* Preformatted Text in Gemini documents

## Stuff that probably should be done differently
* Handling other character encodings than utf-8
* Implement some form of TOFU for self signed certificates
* Probably a lot