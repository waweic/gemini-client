package rocks.ism.decentral.geminiclient

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.method.LinkMovementMethod
import android.text.style.BulletSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.text.style.URLSpan
import android.widget.TextView
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.StringReader
import java.util.*

class GeminiRenderer (private val textView: TextView, private val geminiClient: GeminiClient) {
    private var preRenderMode = false

    fun render(resultBody: String, mimetype: String) {
        CoroutineScope(Dispatchers.Main).launch {
            textView.text = ""
            textView.movementMethod = LinkMovementMethod.getInstance()
            if (mimetype == "text/gemini") {
                renderGemini(resultBody)
            } else {
                renderPlaintext(resultBody)
            }
        }
    }

    fun renderStream(inputStream: InputStream, mimetype: String) {
        //val scanner = Scanner(inputStream, "utf-8")

        CoroutineScope(Dispatchers.Main).launch {
            textView.text = ""
            textView.movementMethod = LinkMovementMethod.getInstance()

            /*
            var line: String?

            do {
                line = GlobalScope.async {
                    if(scanner.hasNextLine()) {
                        scanner.nextLine()
                    } else {
                        null
                    }
                }.await()
                line?.let {renderLine(it, mimetype)}
            } while(line != null)
             */

            val allLines = GlobalScope.async {
                BufferedReader (
                    inputStream.reader()
                ).readLines()
            }
            allLines.await().forEach {
                renderLine(it, mimetype)
            }
        }
    }

    private fun renderLine(line: String, mimetype: String) {
        if(mimetype == "text/gemini") {
            renderGeminiLine(line)
        }
        else {
            textView.append(line + "\n")
        }
    }
    private fun renderGemini(resultBody: String) {
        val bodyReader = BufferedReader(StringReader(resultBody))
        preRenderMode = false
        var line: String? = null
        while ({ line = bodyReader.readLine(); line }() != null) {
            line?.let {
                renderGeminiLine(it)
            }
        }
    }

    private fun renderGeminiLine (line: String) {
        textView.append(when {
            line.startsWith("###") -> {
                val spannable = SpannableString(line.substring(3).trim())
                spannable.setSpan(StyleSpan(Typeface.BOLD), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable
            }
            line.startsWith("##") -> {
                val spannable = SpannableString(line.substring(2).trim())
                spannable.setSpan(StyleSpan(Typeface.BOLD), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable.setSpan(RelativeSizeSpan(1.1f),0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable
            }
            line.startsWith("#") -> {
                val spannable = SpannableString(line.substring(1).trim())
                spannable.setSpan(StyleSpan(Typeface.BOLD), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable.setSpan(RelativeSizeSpan(1.2f),0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable
            }
            line.startsWith("=>") -> {
                val linkParts = line.substring(2).trim().split("\\s+".toRegex(), 2)
                val linkUrl = geminiClient.connection.currentURI?.resolve(linkParts[0]).toString()
                var linkName = linkParts[0]
                if(linkParts.size > 1) {
                    linkName = linkParts[1]
                }
                val spannable = SpannableString(linkName)
                spannable.setSpan(URLSpan(linkUrl), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable
            }
            line.startsWith("*") -> {
                val spannable = SpannableString(line.substring(1))
                spannable.setSpan(BulletSpan(), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
                spannable
            }
            else -> {
                line
            }
        })
        textView.append("\n")
    }

    private fun renderPlaintext(resultBody: String) {
        textView.text = resultBody
    }

}