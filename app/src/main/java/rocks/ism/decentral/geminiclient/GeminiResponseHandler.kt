package rocks.ism.decentral.geminiclient

import android.content.Context
import android.content.Intent
import android.net.Uri
import kotlinx.coroutines.*

class GeminiResponseHandler (private val renderer: GeminiRenderer, private val context: Context, private val geminiClient: GeminiClient) {
    private var numberOfConsecutiveRedirects = 0

    suspend fun handle (deferredResult: Deferred<GeminiResult>) {
            val result = deferredResult.await()

            if(result.header.startsWith("3")) {
                //TODO: Check stuff (like whether we are redirecting to a page without encryption)
                if(++numberOfConsecutiveRedirects > 5) {
                    renderer.render("ERROR: Too many redirections","text/plain")
                }
                else {
                    geminiClient.removeLastHistoryItem() //Remove the link that was originally clicked so the Back-Button works
                    val newUriString = result.connection.currentURI?.resolve(result.meta).toString()
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(newUriString),
                        context,
                        MainActivity::class.java
                    )
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    context.startActivity(intent) //TODO: Is this dangerous? Possibly, the camera could probably be opened with this
                }
            }
            else if(result.header.startsWith("2")){
                numberOfConsecutiveRedirects = 0
                if(result.meta == "text/gemini") {
                    renderer.renderStream(result.body, "text/gemini")
                } else if (result.meta.startsWith("text/")) {
                    renderer.renderStream(result.body,"text/plain")
                }
            }
            else if(result.header.startsWith("1")){
                geminiClient.connection.currentURI?.let { geminiClient.inputRequest(it, result.meta) }
            }
            else {
                renderer.render(result.header,"text/plain")
            }
    }
}