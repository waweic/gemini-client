package rocks.ism.decentral.geminiclient

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.preference.PreferenceManager
import kotlinx.coroutines.*
import java.net.URI

class GeminiClient(val applicationContext: Context, val mainContentView: TextView, val uriField: EditText) {
    val connection : GeminiConnection = GeminiConnection()
    val renderer : GeminiRenderer = GeminiRenderer(mainContentView, this)
    val responseHandler : GeminiResponseHandler = GeminiResponseHandler(renderer, applicationContext, this)
    val sharedPreferences= PreferenceManager.getDefaultSharedPreferences(applicationContext)

    private val history: MutableList<URI> = mutableListOf()

    init {
        PreferenceManager.setDefaultValues(applicationContext, R.xml.preferences, false)
    }

    fun goToPage(uri: URI) {
        if(uri.toString() == "about:preferences") {
            val intent = Intent(applicationContext, SettingsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            applicationContext.startActivity(intent)
        }
        if(history.size == 0 || history[history.size - 1] != uri) {
            history.add(uri) //No double entries. Useful for reloading and going back //TODO: Make this comparison more reproducible
        }
        uriField.setText(uri.toString())
        val deferredResult = GlobalScope.async { connection.request(uri) }
        GlobalScope.launch { responseHandler.handle(deferredResult) }
    }

    fun goToPage(uri: String) {
        //TODO: Do checking and stuff
        goToPage(URI.create(uri))
    }

    fun inputRequest(uri: URI, request: String) {
        val alertBuilder = AlertDialog.Builder(mainContentView.context)
        val inflater = LayoutInflater.from(mainContentView.context)
        alertBuilder.setTitle(R.string.input_request_title)
        val dialogLayout = inflater.inflate(R.layout.input_request_dialog, null)
        val editText = dialogLayout.findViewById<EditText>(R.id.editText)
        editText.hint = request
        alertBuilder.setView(dialogLayout)
        //alertBuilder.setMessage(request)
        alertBuilder.setPositiveButton(R.string.go_button_text) { dialogInterface, i ->
            val newURI = URI(uri.scheme, uri.authority, uri.path, editText.text.toString(), uri.fragment) //TODO: User Info?
            goToPage(newURI)
        }
        alertBuilder.setNegativeButton(R.string.back_button_text) { dialogInterface, i ->
            goBack()
        }
        CoroutineScope(Dispatchers.Main).launch {
            alertBuilder.show()
        }
    }

    fun removeLastHistoryItem() {
        history.removeAt(history.size - 1)
    }
    fun goBack(){
        removeLastHistoryItem()
        goToPage(history[history.size - 1]) //There has to be at least one item in the history //TODO: Double-check this
    }
}