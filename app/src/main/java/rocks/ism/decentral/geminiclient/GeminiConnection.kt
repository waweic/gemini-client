package rocks.ism.decentral.geminiclient

import java.io.*
import java.net.URI
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*
import javax.security.cert.CertificateException


class GeminiConnection {
    var currentURI : URI? = null
        private set
    fun request(uri: URI): GeminiResult {
        currentURI = uri
        if(uri.scheme != "gemini") {
            return GeminiResult(this, GeminiConnectionState.FAILED_WRONG_URI_SCHEME)
        }

        var port = 1965
        if(uri.port != -1) {
            port = uri.port
        }

        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(
            object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                    return arrayOf()
                }
            }
        )

        val sslContext = SSLContext.getInstance("TLSv1.2")
        sslContext.init(null, trustAllCerts, SecureRandom())

        val factory: SSLSocketFactory = sslContext.socketFactory   //SSLSocketFactory.getDefault() as SSLSocketFactory
        val socket: SSLSocket = factory.createSocket(uri.host, port) as SSLSocket
        socket.enabledProtocols = arrayOf("TLSv1.2")

        socket.startHandshake()

        val outWriter = PrintWriter(
            BufferedWriter(
                OutputStreamWriter(
                    socket.outputStream
                )
            )
        )

        outWriter.print(uri.toString() + "\r\n") //TODO: See if I can set <CR><LF> globally
        outWriter.flush()

        if (outWriter.checkError()) {
            return GeminiResult(this, GeminiConnectionState.FAILED_PRINTER_WRITER)
        }

        val inReader = BufferedReader(
            InputStreamReader(
                socket.inputStream
            )
        )

        val responseHeader: String = inReader.readLine()
        //val responseBody: String = inReader.readText()

        inReader.close()
        outWriter.close()
        /*
        socket.close()
         */

        return GeminiResult(this, GeminiConnectionState.SUCCESS, responseHeader, socket.inputStream)
    }
}

enum class GeminiConnectionState {
    FAILED,
    FAILED_WRONG_URI_SCHEME,
    FAILED_PRINTER_WRITER,
    SUCCESS
}

class GeminiResult(val connection: GeminiConnection, val connectionState: GeminiConnectionState) {
    var header: String = ""
        private set

    var statusCode: Int = 0
        private set

    var meta: String = ""
        private set

    lateinit var body: InputStream
        private set

    constructor (connection: GeminiConnection, connectionState: GeminiConnectionState, header: String) : this(connection, connectionState) {
        this.header = header
        val headerParts = header.split("\\s+".toRegex(), 2)
        print(headerParts)
        this.statusCode = headerParts[0].toInt() //TODO: Check if not an int
        this.meta = headerParts[1]
    }
    constructor (connection: GeminiConnection, connectionState: GeminiConnectionState, header: String, body: InputStream) : this(connection, connectionState, header) {
        this.body = body
    }

}