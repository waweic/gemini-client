package rocks.ism.decentral.geminiclient

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var backButton: Button
    private lateinit var goButton: Button
    private lateinit var uriField: EditText
    private lateinit var mainContentView: TextView

    private lateinit var geminiClient: GeminiClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Find UI elements

        backButton = findViewById(R.id.back_button)
        goButton = findViewById(R.id.go_button)
        uriField = findViewById(R.id.gemini_uri)
        mainContentView = findViewById(R.id.mainContentView)

        geminiClient = GeminiClient(
            applicationContext,
            mainContentView,
            uriField
        )

        // Create handlers
        goButton.setOnClickListener {
            geminiClient.goToPage(uriField.text.toString())

            // Hide keyboard
            uriField.clearFocus()
            val inputService = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputService.hideSoftInputFromWindow(uriField.windowToken, 0)
        }

        uriField.setOnKeyListener { _, keyCode, event ->
            if (event?.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                goButton.callOnClick()

                true
            } else false
        }

        backButton.setOnClickListener {
            geminiClient.goBack()
        }

        geminiClient.sharedPreferences.getString("start_page_input", getString(R.string.default_url))?.let {
            geminiClient.goToPage(
                it
            )
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.data.toString().let{geminiClient.goToPage(it)}
    }

    override fun onBackPressed() {
        geminiClient.goBack()
    }
}
